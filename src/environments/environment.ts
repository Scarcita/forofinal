// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  firebase: {
    projectId: 'foro-alcaldia',
    appId: '1:964723128147:web:0b5e843775f8353effd3a6',
    storageBucket: 'foro-alcaldia.appspot.com',
    apiKey: 'AIzaSyBnqaqOXEufQ5afhvKECjw6d6kyXGFEtY8',
    authDomain: 'foro-alcaldia.firebaseapp.com',
    messagingSenderId: '964723128147',
    measurementId: 'G-D3R03LBGT3',
  },
  production: false
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
