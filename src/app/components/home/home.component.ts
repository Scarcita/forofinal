import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  like: number = 0;
  dislike: number = 0;
  user$ = this.authService.currentUser$;
  constructor(private authService: AuthenticationService) { }

  ngOnInit(): void {
  }

  contadorLike() {
    this.like = this.like + 1;
    return this.like;
  }

  contadorDislike() {
    this.dislike = this.dislike + 1;
    return this.dislike;
  }
}
