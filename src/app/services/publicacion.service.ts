import { Injectable } from '@angular/core';
import { IPublicacion } from '../models/publicacion.interface';

@Injectable({
  providedIn: 'root'
})
export class PublicacionService {
  post:IPublicacion[]=[];
  constructor() {
    this.post=[
      {titulo:'Contaminacion Ambiental',fecha:new Date(),descripcion:'Causas de la contaminación ambiental. Tala excesiva de árboles. Emisiones y vertidos industriales a la atmósfera y a la hidrosfera. Extracción, procesamiento y refinamiento de combustibles fósiles (petróleo, carbón y gas natural). Producción de energía con combustibles fósiles y otras fuentes no renovables'},
      
      {titulo:'Tala de arboles',fecha:new Date(),descripcion:'En el planeta hay más de 7,7 millones de especies y más del 20% está en peligro de extinción, tal y como señala National Geographic. El calentamiento global: es una de las principales consecuencias de la deforestación, dado que, sin árboles, el CO2 permanece en la atmósfera y se produce el conocido efecto invernader.'}
    ];
  }

  obtenerPost(){
    return this.post;
  }

  addPost(publicacion:IPublicacion){
    this.post.push(publicacion);
    return false;
  }
}
